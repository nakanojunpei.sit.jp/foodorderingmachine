import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JLabel topLabel;
    private JButton mosButton;
    private JButton teriyakiButton;
    private JButton cheeseButton;
    private JButton vegetableButton;
    private JButton fishButton;
    private JButton chickenButton;
    private JPanel root;
    private JButton checkOutButton;
    private JTextArea receivedOrder;
    private JButton undoButton;
    private JButton redoButton;
    private JButton resetButton;
    private JLabel totalPrice;

    int total = 0;
    String currentText;
    String lastText;
    int currentPrice;
    int lastPrice;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food,int price) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible."
            );
            currentText = receivedOrder.getText();
            receivedOrder.setText(currentText + food + " " + price + "yen\n");

            currentPrice = total;
            total = total + price;

            totalPrice.setText("total " + total + "yen");
        }
    }
    public void checkOut(){
        int checkOut = JOptionPane.showConfirmDialog(
                null,
                "Would you like to checkout?",
                "Checkout Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(checkOut == 0){
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you.The total price is " + total + "yen"
            );
            total = 0;
            receivedOrder.setText(null);
            totalPrice.setText("total " + total + "yen");
            currentText = null;
            lastText = null;
            currentPrice = 0;
            lastPrice = 0;
        }
    }
public FoodOrderingMachine() {
        totalPrice.setText("total " + total + "yen");
        mosButton.setIcon(new javax.swing.ImageIcon(".\\icon\\mos.jpg"));
    mosButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Mos",440);
        }
    });
    teriyakiButton.setIcon(new javax.swing.ImageIcon(".\\icon\\teriyaki.jpg"));
    teriyakiButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Teriyaki",430);
        }
    });
    cheeseButton.setIcon(new javax.swing.ImageIcon(".\\icon\\cheese.jpg"));
    cheeseButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Cheese",280);
        }
    });
    vegetableButton.setIcon(new javax.swing.ImageIcon(".\\icon\\vegetable.jpg"));
    vegetableButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Vegetable",400);
        }
    });
    fishButton.setIcon(new javax.swing.ImageIcon(".\\icon\\fish.jpg"));
    fishButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Fish",390);
        }
    });
    chickenButton.setIcon(new javax.swing.ImageIcon(".\\icon\\chicken.jpg"));
    chickenButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Chicken",360);
        }
    });
    checkOutButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            checkOut();
        }
    });
    undoButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int undo = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to undo?",
                    "Undo",
                    JOptionPane.YES_NO_OPTION
            );
            if(undo == 0) {
                lastText = receivedOrder.getText();
                receivedOrder.setText(currentText);
                lastPrice = total;
                totalPrice.setText("total " + currentPrice + "yen");
                total = currentPrice;
            }
        }
    });
    redoButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(lastPrice != 0) {
                int redo = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to redo?",
                        "Redo",
                        JOptionPane.YES_NO_OPTION
                );
                if(redo == 0) {
                    currentText = receivedOrder.getText();
                    receivedOrder.setText(lastText);
                    currentPrice = total;
                    totalPrice.setText("total " + lastPrice + "yen");
                    total = lastPrice;
                }
            }
        }
    });
    resetButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int reset = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to reset?",
                    "Reset",
                    JOptionPane.YES_NO_OPTION
            );
            if(reset == 0){
                JOptionPane.showMessageDialog(
                        null,
                        "Reset is completed."
                );
                total = 0;
                receivedOrder.setText(null);
                totalPrice.setText("total " + total + "yen");
                currentText = null;
                lastText = null;
                currentPrice = 0;
                lastPrice = 0;
            }
        }
    });
}
}
